package br.ademir.walmart.business;

import br.ademir.walmart.domain.Route;

public interface LogisticsBusiness {
	
	public void processarRota(Route route);

}
