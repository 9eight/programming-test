package br.ademir.walmart.business;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ademir.walmart.database.DbWriter;
import br.ademir.walmart.domain.Route;
import br.ademir.walmart.factory.ApplicationFactory;
import br.ademir.walmart.util.StringUtils;

/**
 * LogisticsBusiness Class
 * 
 * This class implements the BusinessObject JEE Design Pattern
 * 
 * @author Ademir Constantino - <ademirconstantino@gmail.com>
 */
public class LogisticsBusinessImpl implements LogisticsBusiness {

	private Logger logger = LoggerFactory
			.getLogger(LogisticsBusinessImpl.class);

	private DbWriter dbWriter;

	Map<String, Double> malha = new HashMap<String, Double>();
	private StringUtils stringUtils = new StringUtils();

	public LogisticsBusinessImpl() {
		malha.put("AB", 10d);
		malha.put("BD", 15d);
		malha.put("AC", 20d);
		malha.put("CD", 30d);
		malha.put("BE", 50d);
		malha.put("DE", 30d);

		this.dbWriter = new ApplicationFactory().newDbWriterInstance();
	}

	@Override
	public void processarRota(Route route) {
		Long start = System.currentTimeMillis();

		Boolean reversa = stringUtils.string2char(route.getOrigem()) > stringUtils
				.string2char(route.getDestino());
		// se a rota for reversa, ex: E -> A, inverte o inicio e fim
		if (reversa) {
			String troca = route.getOrigem();
			route.setOrigem(route.getDestino());
			route.setDestino(troca);
		}
		route.setRotaPercorrida(route.getOrigem());
		char atual = route.getOrigem().charAt(0);
		atual++;
		traceRoute(route, String.valueOf(atual), route.getDestino());

		Long end = System.currentTimeMillis();
		route.setProcessada(true);
		route.setValorEntrega((route.getKmPercorridos() * route.getValorLitro())
				/ route.getAutonomia());

		if (reversa)
			route.setRotaPercorrida(new StringBuffer(route.getRotaPercorrida())
					.reverse().toString());
		logger.info("Distancia: " + route.getKmPercorridos());
		logger.info("Rota Percorrida: " + route.getRotaPercorrida());
		logger.info("Tempo de processamento: " + (end - start)
				+ " milisegundos");
		try {
			dbWriter.writeRoute(route);
		} catch (SQLException e) {
			logger.error("Ocorreu um erro ao tentar gravar no banco de dados", e);
		}
	}

	/**
	 * Method responsible for processing the route
	 * 
	 * @param route
	 * @param atual
	 */
	private void traceRoute(Route route, String atual, String destino) {
		// caso n�o encontre, iterar todas as rotas
		char inicio = ((char) stringUtils.string2char(route.getOrigem()));
		char atualChar = ((char) stringUtils.string2char(atual));
		Double distance = malha.get(String.valueOf(inicio) + destino);
		if (distance != null) {
			logger.info("Rota encontrada: " + String.valueOf(inicio) + destino
					+ " [" + route.getNmMalha() + "]");
			route.setKmPercorridos(distance);
			route.setRotaPercorrida(route.getRotaPercorrida() + destino);
			route.setProcessada(true);
		}

		String lastFound = String.valueOf(inicio);
		while (!route.getProcessada()) {

			distance = malha.get(String.valueOf(inicio) + destino);
			if (distance != null) {
				logger.info("Rota encontrada: " + String.valueOf(inicio)
						+ destino);
				route.setKmPercorridos(route.getKmPercorridos() + distance);
				route.setRotaPercorrida(route.getRotaPercorrida() + destino);
				route.setProcessada(true);
			}

			distance = malha.get(String.valueOf(inicio) + atualChar);
			if (distance != null && String.valueOf(inicio).equals(lastFound)) {
				logger.info("Rota encontrada: " + String.valueOf(inicio)
						+ atualChar + " [" + route.getNmMalha() + "]");
				route.setRotaPercorrida(route.getRotaPercorrida()
						+ String.valueOf(atualChar));
				lastFound = String.valueOf(atualChar);
				inicio = atualChar;
				route.setKmPercorridos(route.getKmPercorridos() + distance);
			}

			atualChar++;
		}
	}

	public void setMalha(Map<String, Double> malha) {
		this.malha = malha;
	}
}
