package br.ademir.walmart.database;

import java.sql.SQLException;

import br.ademir.walmart.domain.Route;

public interface DbWriter {

	public void writeRoute(Route route) throws SQLException;

}
