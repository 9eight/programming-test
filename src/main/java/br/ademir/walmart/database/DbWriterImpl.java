package br.ademir.walmart.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ademir.walmart.domain.Route;

public class DbWriterImpl implements DbWriter {

	private Connection connection;
	private static Logger logger = LoggerFactory.getLogger(DbWriterImpl.class);

	public DbWriterImpl() {
		try {
			createRouteTable();
		} catch (SQLException e) {
			logger.error("Erro ao tentar criar tabela no banco de dados", e);
		}
	}

	static {
		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
		} catch (ClassNotFoundException e) {
			logger.error("Classe HSQLDB n�o pode ser encontrada", e);
		}
	}

	private void initConnection() {
		try {
			connection = DriverManager.getConnection(
					"jdbc:hsqldb:file/code-test/walmart.db;shutdown=true", "SA", "");
		} catch (SQLException e) {
			logger.error("Erro ao tentar criar conex�o com o banco de dados", e);
		}
	}

	private void createRouteTable() throws SQLException {

		if (null == connection || connection.isClosed()) {
			initConnection();
		}

		connection.prepareCall("SET DATABASE DEFAULT TABLE TYPE MEMORY;")
				.execute();
		connection.prepareCall(
				"CREATE TABLE IF NOT EXISTS route ( "
						+ " ID INTEGER NOT NULL IDENTITY, "
						+ " nm_malha VARCHAR (255) NOT NULL, "
						+ " origem VARCHAR (1) NOT NULL, "
						+ " destino VARCHAR (1) NOT NULL, "
						+ " autonomia DECIMAL(10,2) NOT NULL,  "
						+ " valor_litro DECIMAL(10,2) NOT NULL,  "
						+ " processada BOOLEAN NOT NULL,  "
						+ " valor_entrega DECIMAL(10,2) NOT NULL,  "
						+ " km_percorridos DECIMAL(10,2) NOT NULL,  "
						+ " rota_percorrida VARCHAR (255) NOT NULL " + "  );")
				.execute();

		connection.commit();
		connection.close();
	}

	@Override
	public void writeRoute(Route route) throws SQLException {

		if (null == connection || connection.isClosed()) {
			initConnection();
		}

		PreparedStatement ps = connection
				.prepareStatement("INSERT INTO route (nm_malha, origem, destino, autonomia, valor_litro, processada, valor_entrega, km_percorridos, rota_percorrida) VALUES(?,?,?,?,?,?,?,?,?)");
		ps.setString(1, route.getNmMalha());
		ps.setString(2, route.getOrigem());
		ps.setString(3, route.getDestino());
		ps.setDouble(4, route.getAutonomia());
		ps.setDouble(5, route.getValorLitro());
		ps.setBoolean(6, route.getProcessada());
		ps.setDouble(7, route.getValorEntrega());
		ps.setDouble(8, route.getKmPercorridos());
		ps.setString(9, route.getRotaPercorrida());
		ps.executeUpdate();

		connection.commit();
		connection.close();
	}

}
