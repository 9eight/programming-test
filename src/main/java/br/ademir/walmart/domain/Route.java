package br.ademir.walmart.domain;

public class Route {

	private String nmMalha;
	private String origem;
	private String destino;
	private Double autonomia;
	private Double valorLitro;

	private Boolean processada;
	private Double valorEntrega;
	private Double kmPercorridos;
	private String rotaPercorrida;

	public Route(String nmMalha, String origem, String destino,
			Double autonomia, Double valorLitro) {
		setNmMalha(nmMalha);
		setOrigem(origem);
		setDestino(destino);
		setAutonomia(autonomia);
		setValorLitro(valorLitro);
		setRotaPercorrida("");
		setProcessada(false);
		setValorEntrega(0d);
		setKmPercorridos(0d);
	}

	public String getNmMalha() {
		return nmMalha;
	}

	public void setNmMalha(String nmMalha) {
		this.nmMalha = nmMalha;
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public Double getAutonomia() {
		return autonomia;
	}

	public void setAutonomia(Double autonomia) {
		this.autonomia = autonomia;
	}

	public Double getValorLitro() {
		return valorLitro;
	}

	public void setValorLitro(Double valorLitro) {
		this.valorLitro = valorLitro;
	}

	public Boolean getProcessada() {
		return processada;
	}

	public void setProcessada(Boolean processada) {
		this.processada = processada;
	}

	public Double getValorEntrega() {
		return valorEntrega;
	}

	public void setValorEntrega(Double valorEntrega) {
		this.valorEntrega = valorEntrega;
	}

	public Double getKmPercorridos() {
		return kmPercorridos;
	}

	public void setKmPercorridos(Double kmPercorridos) {
		this.kmPercorridos = kmPercorridos;
	}

	public String getRotaPercorrida() {
		return rotaPercorrida;
	}

	public void setRotaPercorrida(String rotaPercorrida) {
		this.rotaPercorrida = rotaPercorrida;
	}

}