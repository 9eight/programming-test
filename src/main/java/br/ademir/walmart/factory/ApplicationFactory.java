package br.ademir.walmart.factory;

import br.ademir.walmart.business.LogisticsBusinessImpl;
import br.ademir.walmart.database.DbWriterImpl;

/**
 * ApplicationFactory Class
 * 
 * This class implements the Factory Design Pattern
 * 
 * @author Ademir Constantino - <ademirconstantino@gmail.com>
 */
public class ApplicationFactory {

	public LogisticsBusinessImpl newBusinessInstance() {
		return new LogisticsBusinessImpl();
	}

	public DbWriterImpl newDbWriterInstance() {
		return new DbWriterImpl();
	}

}
