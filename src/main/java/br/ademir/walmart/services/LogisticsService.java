package br.ademir.walmart.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

import br.ademir.walmart.business.LogisticsBusiness;
import br.ademir.walmart.domain.Route;
import br.ademir.walmart.factory.ApplicationFactory;

/**
 * LogisticsService Class
 * 
 * This class is a REST Service - This class is instantiated by the Jersey
 * Framework
 * 
 * @author Ademir Constantino - <ademirconstantino@gmail.com>
 */
@Provider
@Path("/logisticsService")
public class LogisticsService {

	private LogisticsBusiness business;

	public LogisticsService() {
		this.business = new ApplicationFactory().newBusinessInstance();
	}

	@GET
	@Path("/getRouteDistance")
	@Produces(MediaType.APPLICATION_JSON)
	public Route getRouteDistance(@QueryParam("nomeMalha") String nmMalha,
			@QueryParam("origem") String origem,
			@QueryParam("destino") String destino,
			@QueryParam("autonomia") Double autonomia,
			@QueryParam("valorLitro") Double valorLitro) {

		Boolean validate = this.validateInput(nmMalha, origem, destino,
				autonomia, valorLitro);

		if (!validate) {
			return null;
		}

		Route route = new Route(nmMalha, origem, destino, autonomia, valorLitro);
		business.processarRota(route);
		return route;
	}

	/**
	 * Validate input fields
	 * 
	 * @param nmMalha
	 * @param origem
	 * @param destino
	 * @param autonomia
	 * @param valorLitro
	 * 
	 * @return true if validated
	 */
	private boolean validateInput(String nmMalha, String origem,
			String destino, Double autonomia, Double valorLitro) {
		return ((nmMalha != null && !nmMalha.isEmpty())
				&& (origem != null && !origem.isEmpty())
				&& (destino != null && !destino.isEmpty())
				&& (autonomia != null && autonomia > 0d)
				&& (valorLitro != null && valorLitro > 0d) && !origem
					.equals(destino));
	}

	public LogisticsBusiness getBusiness() {
		return business;
	}

}
