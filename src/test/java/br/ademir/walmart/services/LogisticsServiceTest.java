package br.ademir.walmart.services;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import br.ademir.walmart.business.LogisticsBusinessImpl;
import br.ademir.walmart.domain.Route;

@RunWith(JUnit4.class)
public class LogisticsServiceTest extends TestCase {

	private LogisticsService service;

	@Before
	public void init() {
		service = new LogisticsService();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testNullAllInputs() {
		assertNull(service.getRouteDistance(null, null, null, null, null));
	}

	@Test
	public void testNameInput() {
		assertNull(service.getRouteDistance("SP", null, null, null, null));
	}

	@Test
	public void testInvalidInput() {
		assertNull(service.getRouteDistance("SP", "SP", "SP", 0d, 2.5d));
	}
	
	@Test
	public void testRouteA2Z() {
		((LogisticsBusinessImpl) service.getBusiness()).setMalha(getMalha1());
		Route a = service.getRouteDistance("SP", "A", "Z", 10.0d, 2.5d);
		assertEquals("ABDEFGHOZ", a.getRotaPercorrida());
	}
	
	@Test
	public void testRouteZ2A() {
		((LogisticsBusinessImpl) service.getBusiness()).setMalha(getMalha1());
		Route a = service.getRouteDistance("SP", "Z", "A", 10.0d, 2.5d);
		assertEquals("ZOHGFEDBA", a.getRotaPercorrida());
	}


	@Test
	public void testRouteAtoD() {
		((LogisticsBusinessImpl) service.getBusiness()).setMalha(getMalha2());
		Route a = service.getRouteDistance("SP", "A", "D", 10.0d, 2.5d);
		assertEquals("ABD", a.getRotaPercorrida());
	}
	
	@Test
	public void testRouteAtoC() {
		((LogisticsBusinessImpl) service.getBusiness()).setMalha(getMalha2());
		Route a = service.getRouteDistance("SP", "A", "C", 10.0d, 2.5d);
		assertEquals("AC", a.getRotaPercorrida());
	}
	
	@Test
	public void testValorEntregaRouteAtoD() {
		((LogisticsBusinessImpl) service.getBusiness()).setMalha(getMalha2());
		Route a = service.getRouteDistance("SP", "A", "D", 10.0d, 2.5d);
		assertEquals(6.25, a.getValorEntrega());
	}

	private Map<String, Double> getMalha1() {
		Map<String, Double> malha = new HashMap<String, Double>();
		malha.put("AB", 10d);
		malha.put("BD", 10d);
		malha.put("AC", 10d);
		malha.put("CD", 10d);
		malha.put("BE", 10d);
		malha.put("DE", 10d);
		malha.put("EF", 10d);
		malha.put("FG", 10d);
		malha.put("GH", 1000d);
		malha.put("HO", 1000d);
		malha.put("OZ", 1000d);
		return malha;
	}

	private Map<String, Double> getMalha2() {
		Map<String, Double> malha = new HashMap<String, Double>();
		malha.put("AB", 10d);
		malha.put("BD", 15d);
		malha.put("AC", 20d);
		malha.put("CD", 30d);
		malha.put("BE", 50d);
		malha.put("DE", 30d);
		return malha;
	}

}